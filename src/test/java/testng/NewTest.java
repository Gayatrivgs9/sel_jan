package testng;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.AfterSuite;

public class NewTest {
  @Test
  public void f() {
	  System.out.println("I am Test");
  }
  @BeforeMethod
  public void beforeMethod() {
	  System.out.println("I am Beforemethod");
  }

  @AfterMethod
  public void afterMethod() {
	  System.out.println("I am Aftermethod");
  }

  @BeforeClass
  public void beforeClass() {
	  System.out.println("I am Beforeclass");
  }

  @AfterClass
  public void afterClass() {
	  System.out.println("I am afterclass");
  }

  @BeforeTest
  public void beforeTest() {
	  System.out.println("I am BeforeTest");
  }

  @AfterTest
  public void afterTest() {
	  System.out.println("I am afterTest");
  }

  @BeforeSuite
  public void beforeSuite() {
	  System.out.println("I am beforesuite");
  }

  @AfterSuite
  public void afterSuite() {
	  System.out.println("I am aftersuite");
  }

}
