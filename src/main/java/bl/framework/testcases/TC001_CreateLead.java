package bl.framework.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import bl.framework.design.ProjectMethods;

public class TC001_CreateLead extends ProjectMethods{
	
	@BeforeTest
	public void setData() {
		testcaseName = "TC001_CreateLead";
		testDec = "Create a new Lead in leaftaps";
		author = "Gayatri";
		category = "Smoke";
		dataSheetName = "TC001";
	} 
	//@Test(dependsOnMethods= {"bl.framework.testcases.TC002_EditLead"})
	
	@Test(dataProvider="getData", invocationCount=2)
	public void createLead(String cname, String fname, String lname, int data) {
		click(locateElement("link", "Leads1"));
		click(locateElement("link", "Create Lead"));
		clearAndType(locateElement("id", "createLeadForm_companyName"), cname);
		clearAndType(locateElement("id", "createLeadForm_firstName"), fname);
		clearAndType(locateElement("id", "createLeadForm_lastName"), ""+data);
		System.out.println(data);
		click(locateElement("name", "submitButton")); 
	}
	
	@DataProvider(name="getData", indices= 0)
	public Object[][] fetchdata() {
		Object[][] data = new Object[2][4];
		data[0][0] = "TestLeaf";
		data[0][1] = "Balaji";
		data[0][2] = "C";
		data[0][3] = 9876543;
		
		data[1][0] = "TestLeaf";
		data[1][1] = "Koushik";
		data[1][2] = "C";
		data[1][3] = 12345;
		return data;
	}
	@DataProvider(name="getData1")
	public String[][] fetchdata1() {
		String[][] data = new String[2][3];
		data[0][0] = "TestLeaf";
		data[0][1] = "Sharath";
		data[0][2] = "C";
		
		data[1][0] = "TestLeaf";
		data[1][1] = "Gayathri";
		data[1][2] = "C";
		return data;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
/*//	@Test(priority = 2)
//	@Test(enabled = false)
	public void createLead1() {
		click(locateElement("link", "Leads"));
		click(locateElement("link", "Create Lead"));
		clearAndType(locateElement("id", "createLeadForm_companyName"), "TL");
		clearAndType(locateElement("id", "createLeadForm_firstName"), "Koushik");
		clearAndType(locateElement("id", "createLeadForm_lastName"), "Ch");
		click(locateElement("name", "submitButton")); 
	}*/
	
	
	
}







